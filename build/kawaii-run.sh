#!/bin/sh
export SIB3D_PATH="$(pwd)/distr/lib/Kawaii3D:$(pwd)/distr/share/Kawaii3D"
export LD_LIBRARY_PATH="$(pwd)/distr/lib"
export GALLIUM_HUD='simple,frametime,GPU-load+GPU-shaders-busy,VRAM-usage'
export SIB3D_VSYNC=0
$@

