#include "MainWindow.hpp"
#include "ui_MainWindow.h"

#include <QDir>
#include <QProcess>
#include <QFileDialog>
#include <QMessageBox>
#include <QStandardPaths>
#include <QCoreApplication>
#include <QProcessEnvironment>

#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonObject>

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow),
  declaredMemento(false)
{
  ui->setupUi(this);

  ui->listPluginDirs->setModel(&pluginDirs);

  connect(&pluginDirs, &QStringListModel::rowsInserted, this, &MainWindow::detectRendererPlugins);
  connect(&pluginDirs, &QStringListModel::rowsRemoved, this, &MainWindow::detectRendererPlugins);

  QDir d(QCoreApplication::instance()->applicationDirPath());
  d.cdUp();
  if(d.cd("lib") && d.cd("Kawaii3D"))
    {
      pluginDirs.insertRow(0);
      pluginDirs.setData(pluginDirs.index(0), d.absolutePath());
      d.cdUp();
      d.cdUp();
    }
  if(d.cd("share") && d.cd("Kawaii3D"))
    {
      pluginDirs.insertRow(1);
      pluginDirs.setData(pluginDirs.index(1), d.absolutePath());
    }

  QLabel *statusLabel = new QLabel(QStringLiteral("готов"));
  connect(&gameProcess, &QProcess::stateChanged, [statusLabel] (QProcess::ProcessState state) {
    switch(state) {
      case QProcess::Starting:
        statusLabel->setText(QStringLiteral("игра запускается..."));
        break;

      case QProcess::Running:
        statusLabel->setText(QStringLiteral("в игре"));
        break;

      case QProcess::NotRunning:
        statusLabel->setText(QStringLiteral("готов"));
        break;
      }
  });

  connect(&gameProcess, &QProcess::errorOccurred, [statusLabel] (QProcess::ProcessError err) {
    switch(err) {
      case QProcess::ProcessError::FailedToStart:
        statusLabel->setText(QStringLiteral("ошибка запуска!"));
        break;

      case QProcess::ProcessError::Crashed:
        statusLabel->setText(QStringLiteral("ошибка выполнения!"));
        break;

      case QProcess::ProcessError::Timedout:
        statusLabel->setText(QStringLiteral("истёк таймаут!"));
        break;

      default:
        statusLabel->setText(QStringLiteral("неизвестная ошибка!"));
        break;
      }
  });

  ui->statusbar->addPermanentWidget(statusLabel);

  connect(&gameProcess, &QProcess::readyReadStandardOutput, this, &MainWindow::printProcessOutput);
  connect(&gameProcess, &QProcess::readyReadStandardError, this, &MainWindow::printProcessOutput);
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::on_btnPlay_clicked()
{
  if(gameProcess.state() != QProcess::NotRunning)
    {
      QMessageBox::critical(this, QStringLiteral(APP_NAME": Информация"), QStringLiteral("Игра уже запущена"));
      return;
    }

  QFileInfo binFile(QDir(QCoreApplication::instance()->applicationDirPath()).absoluteFilePath(QStringLiteral("TheChoice")));
  if(!binFile.isFile())
    binFile.setFile(QDir(QCoreApplication::instance()->applicationDirPath()).absoluteFilePath(QStringLiteral("TheChoice.exe")));

  if(!binFile.isFile())
    {
      QMessageBox::critical(this, QStringLiteral(APP_NAME": Критическая ошибка"), QStringLiteral("Исполняемый файл игры не найден!"));
      return;
    }

  if(!binFile.isExecutable())
    {
      QMessageBox::critical(this, QStringLiteral(APP_NAME": Критическая ошибка"), QStringLiteral("Исполняемый файл игры не доступен для запуска!"));
      return;
    }

  gameProcess.setProgram(binFile.filePath());
  gameProcess.setArguments( {ui->lineEditFile->text()} );

  QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
  const auto sib3d_path = pluginDirs.stringList().join(':');
  env.insert("SIB3D_PATH", sib3d_path);

  QDir d(QCoreApplication::instance()->applicationDirPath());
  d.cdUp();
  if(d.cd("lib"))
    env.insert("LD_LIBRARY_PATH", d.absolutePath());

  if(ui->combo_selectRenderer->count())
    env.insert("SIB3D_RENDERER", ui->combo_selectRenderer->currentText());

  gameProcess.setProcessEnvironment(env);

  gameProcess.start();
}

void MainWindow::on_lineEditFile_textChanged(const QString &arg1)
{
  ui->browserParseErrors->clear();

  QFileInfo storyFile(arg1);
  if(!storyFile.isFile())
    {
      ui->browserParseErrors->append(QStringLiteral("Файл %1 не существует").arg(arg1));
      return;
    }

  if(!storyFile.isReadable())
    {
      ui->browserParseErrors->append(QStringLiteral("Файл %1 не доступен для чтения").arg(arg1));
      return;
    }

  QJsonParseError parseError;
  QJsonObject jsonObject;
  try {
    QFile f(storyFile.filePath());
    f.open(QFile::ReadOnly | QFile::Text);
    const auto storyBytes = f.readAll();
    f.close();
    jsonObject = QJsonDocument::fromJson(storyBytes, &parseError).object();
  } catch(...) {
    ui->browserParseErrors->append(QStringLiteral("Файл %1 не читается").arg(arg1));
    return;
  }

  if(parseError.error != QJsonParseError::NoError)
    ui->browserParseErrors->append(parseError.errorString());

  if(auto respack_json = jsonObject.value("resource_pack_path"); respack_json.isString())
    declaredMemento = QFile(storyFile.dir().absoluteFilePath(respack_json.toString())).exists();
  else
    declaredMemento = false;

  QString player_model;
  if(auto val = jsonObject.value("player_model"); !val.isString() && !declaredMemento)
    ui->browserParseErrors->append("Invalid JSON: must contain \'player_model\' string");

  if(auto models = jsonObject.value("models"); models.isArray())
    checkModels(models.toArray());
  else if(!declaredMemento)
    ui->browserParseErrors->append(QStringLiteral("Invalid JSON: must contain \'models\' array"));

  if(auto music_json = jsonObject.value("music"); music_json.isObject())
    checkMusic(music_json.toObject());
  else if(!declaredMemento)
    ui->browserParseErrors->append(QStringLiteral("Invalid JSON: must contain \'music\' object"));

  if(auto endings_json = jsonObject.value("ending_images"); endings_json.isObject())
    checkEndings(endings_json.toObject());
  else
    ui->browserParseErrors->append(QStringLiteral("Invalid JSON: must contain \'ending_images\' object"));

  if(auto killer_types = jsonObject.value("killer_types"); killer_types.isArray())
    checkKillerTypes(killer_types.toArray());
  else
    ui->browserParseErrors->append(QStringLiteral("Invalid JSON: must contain \'killer_types\' array"));

  if(auto bonus_types = jsonObject.value("bonus_types"); bonus_types.isArray())
    checkBonusTypes(bonus_types.toArray());
  else
    ui->browserParseErrors->append(QStringLiteral("Invalid JSON: must contain \'bonus_types\' array"));

  if(auto rooms_json = jsonObject.value("rooms"); rooms_json.isArray())
    checkRooms(rooms_json.toArray());
  else
    ui->browserParseErrors->append(QStringLiteral("Invalid JSON: must contain \'rooms\' array"));

  if(auto story_json = jsonObject.value("story"); story_json.isArray())
    checkStory(story_json.toArray());
  else
    ui->browserParseErrors->append(QStringLiteral("Invalid JSON: must contain \'story\' array"));
}

void MainWindow::checkModels(const QJsonArray &json)
{
  for(const auto &i: json)
    {
      if(!i.isObject())
        {
          ui->browserParseErrors->append(QStringLiteral("Invalid JSON: each elem of \'models\' must be an object"));
          continue;
        }

      const auto modelObj = i.toObject();
      if(!modelObj.value("file").isString())
        ui->browserParseErrors->append(QStringLiteral("Invalid JSON: each elem of \'models\' must contain \'file\' string"));

      if(!modelObj.value("name").isString())
        ui->browserParseErrors->append(QStringLiteral("Invalid JSON: each elem of \'models\' must contain \'name\' string"));

      if(auto scaleVal = modelObj.value("scale"); scaleVal.isArray())
        checkVec3(scaleVal.toArray(), "model.scale");
    }
}

void MainWindow::checkMusic(const QJsonObject &json)
{
  if(auto val = json.value("scratch_file"); !val.isString())
    ui->browserParseErrors->append(QStringLiteral("Invalid JSON: \'music\' object must contain \'scratch_file\' string"));

  if(auto val = json.value("space_file"); !val.isString())
    ui->browserParseErrors->append(QStringLiteral("Invalid JSON: \'music\' object must contain \'space_file\' string"));

  if(auto val = json.value("dystopia_file"); !val.isString())
    ui->browserParseErrors->append(QStringLiteral("Invalid JSON: \'music\' object must contain \'dystopia_file\' string"));

  if(auto val = json.value("requiem_file"); !val.isString())
    ui->browserParseErrors->append(QStringLiteral("Invalid JSON: \'music\' object must contain \'requiem_file\' string"));

  if(auto val = json.value("cyberdance_file"); !val.isString())
    ui->browserParseErrors->append(QStringLiteral("Invalid JSON: \'music\' object must contain \'cyberdance_file\' string"));
}

void MainWindow::checkEndings(const QJsonObject &json)
{
  if(auto val = json.value("scratch_file"); !val.isString())
    ui->browserParseErrors->append(QStringLiteral("Invalid JSON: \'ending_images\' object must contain \'scratch_file\' string"));

  if(auto val = json.value("space_file"); !val.isString())
    ui->browserParseErrors->append(QStringLiteral("Invalid JSON: \'ending_images\' object must contain \'space_file\' string"));

  if(auto val = json.value("dystopia_file"); !val.isString())
    ui->browserParseErrors->append(QStringLiteral("Invalid JSON: \'ending_images\' object must contain \'dystopia_file\' string"));

  if(auto val = json.value("requiem_file"); !val.isString())
    ui->browserParseErrors->append(QStringLiteral("Invalid JSON: \'ending_images\' object must contain \'requiem_file\' string"));

  if(auto val = json.value("cyberdance_file"); !val.isString())
    ui->browserParseErrors->append(QStringLiteral("Invalid JSON: \'ending_images\' object must contain \'cyberdance_file\' string"));
}

void MainWindow::checkKillerTypes(const QJsonArray &json)
{
  for(const auto &i: json)
    {
      if(!i.isObject())
        {
          ui->browserParseErrors->append(QStringLiteral("Invalid JSON: each elem of \'killer_types\' must be an object"));
          continue;
        }

      const auto killerObj = i.toObject();
      if(!killerObj.value("type").isString())
        ui->browserParseErrors->append(QStringLiteral("Invalid JSON: each element of \'killer_types\' array must contain \'type\' string"));

      if(!killerObj.value("model_name").isString())
        ui->browserParseErrors->append(QStringLiteral("Invalid JSON: each element of \'killer_types\' array must contain \'model_name\' string"));

      if(!killerObj.value("speed").isDouble())
        ui->browserParseErrors->append(QStringLiteral("Invalid JSON: each element of \'killer_types\' array must contain \'speed\' number"));
    }
}

namespace {
  const QSet<QString> bonusEffects =
  {
    { QStringLiteral("hp+") },
    { QStringLiteral("karma+") },
    { QStringLiteral("hp-") },
    { QStringLiteral("karma-") }
  };
}

void MainWindow::checkBonusTypes(const QJsonArray &json)
{
  for(const auto &i: json)
    {
      if(!i.isObject())
        {
          ui->browserParseErrors->append(QStringLiteral("Invalid JSON: each elem of \'bonus_types\' must be an object"));
          continue;
        }

      const auto bonusJson = i.toObject();
      if(!bonusJson.value("type").isString())
        ui->browserParseErrors->append(QStringLiteral("Invalid JSON: each element of \'bonus_types\' array must contain \'type\' string"));

      if(!bonusJson.value("model_name").isString())
        ui->browserParseErrors->append(QStringLiteral("Invalid JSON: each element of \'bonus_types\' array must contain \'model_name\' string"));

      if(auto val = bonusJson.value("effect"); val.isString())
        {
          if(!bonusEffects.contains(bonusJson.value("effect").toString().toLower()))
            ui->browserParseErrors->append(QStringLiteral("Invalid JSON: unknown BonusType::Effect \'%1\'").arg(bonusJson.value("effect").toString().toLower()));
        } else
        ui->browserParseErrors->append(QStringLiteral("Invalid JSON: each element of \'bonus_types\' array should contain \'effect\' string"));
    }
}

void MainWindow::checkRooms(const QJsonArray &json)
{
  for(const auto &i: json)
    {
      if(!i.isObject())
        ui->browserParseErrors->append(QStringLiteral("Invalid JSON: each elem of \'rooms\' must be an object"));
    }
}

void MainWindow::checkStory(const QJsonArray &json)
{
  for(const auto &i: json)
    if(!i.isDouble())
      ui->browserParseErrors->append(QStringLiteral("Invalid JSON: each elem of \'story\' must be a number"));

  if(ui->browserParseErrors->toPlainText().isEmpty())
    ui->browserParseErrors->append(QStringLiteral("Сюжет на %1 комнат").arg(json.size()));
}

void MainWindow::checkVec3(const QJsonArray &arr, const QString &name)
{
  if(arr.size() != 3
     || !arr.at(0).isDouble()
     || !arr.at(1).isDouble()
     || !arr.at(2).isDouble())
    {
      ui->browserParseErrors->append(QStringLiteral("Invalid JSON: %1 is expected to be array of 3 numbers").arg(name));
    }
}

void MainWindow::printProcessOutput()
{
  ui->browserLog->append(gameProcess.readAllStandardOutput());
  ui->browserLog->append(QStringLiteral("<span style=\"color:#ab3333;\">%1</span>").arg(QString::fromUtf8(gameProcess.readAllStandardError())));
}

void MainWindow::detectRendererPlugins()
{
  ui->combo_selectRenderer->clear();

  auto resDirs = QList<QDir> {
      QDir(QStringLiteral("/usr/lib/Kawaii3D/")),
      QDir(QStringLiteral("/usr/local/lib/Kawaii3D/")),
      QDir(QStringLiteral("/usr/share/Kawaii3D/")),
      QDir::homePath() + QStringLiteral("/.local/share/Kawaii3D"),
      QDir::currentPath() };

  for(int i = 0; i < pluginDirs.rowCount(); ++i)
    resDirs.push_back(QDir(pluginDirs.data(pluginDirs.index(i)).toString()));

  for(QDir &d: resDirs)
    {
      d.cd(QStringLiteral("Plugins"));
      d.cd(QStringLiteral("Renders"));
      for(const auto &fName: d.entryList({"*.conf"}))
        {
          QFile f(d.absoluteFilePath(fName));
          f.open(QFile::ReadOnly | QFile::Text);
          QJsonDocument jsonDoc = QJsonDocument::fromJson(f.readAll());
          f.close();
          if(auto val = jsonDoc.object().value("plugin_name"); val.isString())
            ui->combo_selectRenderer->addItem(val.toString());
        }
    }
}

void MainWindow::on_btnSelectFile_clicked()
{
  QString fName = QFileDialog::getOpenFileName(this, QStringLiteral("Выбрать файл сюжета"), QString(), QStringLiteral("JSON files (*.json);;All files (*)"));
  if(!fName.isNull())
    ui->lineEditFile->setText(fName);
}

void MainWindow::on_btnAddPluginDir_clicked()
{
  QString fName = QFileDialog::getExistingDirectory(this, QStringLiteral("Выбрать директорию плагинов"), QString());
  if(!fName.isNull())
    {
      int n = pluginDirs.rowCount();
      pluginDirs.insertRows(n, 1);
      pluginDirs.setData(pluginDirs.index(n), fName);
    }
}

void MainWindow::on_btnDelPluginDir_clicked()
{
  if(ui->listPluginDirs->currentIndex().isValid())
    pluginDirs.removeRows(ui->listPluginDirs->currentIndex().row(), 1);
}
