#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>

#include <QProcess>
#include <QStringListModel>

namespace Ui {
  class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

private slots:
  void on_btnPlay_clicked();

  void on_lineEditFile_textChanged(const QString &arg1);

  void on_btnSelectFile_clicked();

  void on_btnAddPluginDir_clicked();

  void on_btnDelPluginDir_clicked();

private:
  Ui::MainWindow *ui;

  QProcess gameProcess;
  QStringListModel pluginDirs;
  bool declaredMemento;

  void checkModels(const QJsonArray &json);
  void checkMusic(const QJsonObject &json);
  void checkEndings(const QJsonObject &json);
  void checkKillerTypes(const QJsonArray &json);
  void checkBonusTypes(const QJsonArray &json);
  void checkRooms(const QJsonArray &json);
  void checkStory(const QJsonArray &json);

  void checkVec3(const QJsonArray &arr, const QString &name);

  void printProcessOutput();

  void detectRendererPlugins();
};

#endif // MAINWINDOW_HPP
