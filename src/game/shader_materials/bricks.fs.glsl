module MaterialEffectFrag;
precision mediump int; precision highp float;

layout(location = 2) in highp vec3 texCoord;

import MaterialInfo from PbrIlluminationCore;

//albedo; metallic; roughness; ao;
MaterialInfo pbr_material;

Material {
    vec4 scale;
    vec4 brickColor;
    vec4 mortarColor;
    vec2 brickSize;
    vec2 brickPct;

    vec2 brickProps;
    vec2 mortarProps;

    float innerSide;
} material;

void material_effect(in vec3 normal, in vec3 worldPos)
{
    if(gl_FrontFacing != (material.innerSide > 0)) { discard; return; }

    const vec3 absNormal = abs(normal);

    vec2 position = texCoord.xy / material.brickSize;

    if(absNormal.x > absNormal.y)
    {
        if(absNormal.x > absNormal.z)
            position = texCoord.zy / material.brickSize;
        else
            position = texCoord.xy / material.brickSize;
    } else
    {
        if(absNormal.y > absNormal.z)
            position = texCoord.xz / material.brickSize;
        else
            position = texCoord.xy / material.brickSize;
    }

    if (fract(position.y * 0.5) > 0.5)
        position.x += 0.5;

    position = fract(position);

    vec2 useBrick = step(position, material.brickPct);

    float use_brick = useBrick.x * useBrick.y;
    pbr_material.albedo = mix(material.mortarColor.rgb, material.brickColor.rgb, use_brick);
    pbr_material.roughness = mix(material.mortarProps.y, material.brickProps.y, use_brick);
    pbr_material.metallic = mix(material.mortarProps.x, material.brickProps.x, use_brick);
    pbr_material.ao = mix(0.33, 1.0, use_brick);
}

export material_effect;
