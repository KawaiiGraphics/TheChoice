#include "Game.hpp"

#include "SolidGlsl.hpp"
#include <Kawaii3D/Surfaces/KawaiiSurface.hpp>
#include <sib_utils/ioReadAll.hpp>

#include <KawaiiWorlds/clockworks/BarierClockwork.hpp>
#include <KawaiiWorlds/clockworks/GravityAccelerationClockwork.hpp>
#include <KawaiiWorlds/clockworks/ViscosityClockwork.hpp>
#include <KawaiiWorlds/clockworks/FrictionClockwork.hpp>
#include <KawaiiWorlds/clockworks/HookesLawClockwork.hpp>
#include <KawaiiWorlds/clockworks/SolidizerClockwork.hpp>

#include <Kawaii3D/Illumination/KawaiiPBR.hpp>

constexpr size_t barierClockworkIndex = 2;

Game::Game():
  music(world),
  ending(MusicMachine::State::Idle),
  frameIndex(0),
  doorHalfHeight(0)
{
  world.updateParent(&root);

  world.addClockwork(std::make_unique<HookesLawClockwork>());
  world.addClockwork(std::make_unique<GravityAccelerationClockwork>(QVector3D(0, -9.8, 0)));

  world.addClockwork(std::make_unique<BarierClockwork>());
  static_cast<BarierClockwork&>(world.getClockwork(barierClockworkIndex)).frictionFactor = 0.16;

  world.addClockwork(std::make_unique<ViscosityClockwork>(0.06));
  world.addClockwork(std::make_unique<FrictionClockwork>());
  world.addClockwork(std::make_unique<SolidizerClockwork>());

  world.createIlluminationModel<KawaiiPbrLight>("PBR");
}

Game::~Game()
{
}

World &Game::getWorld()
{
  return world;
}

void Game::goToRoom(size_t roomI)
{
  roomIndex = roomI;
  if(roomIndex > story.size() - 1)
    return finish();

  clearRoom();

  const auto &room = rooms[story[roomIndex]];

  auto &barier = static_cast<BarierClockwork&>(world.getClockwork(barierClockworkIndex));

  barier.min_x = -room.getSize().x() / 2.0;
  barier.min_y = -room.getSize().y() / 2.0;
  barier.min_z = -room.getSize().z() / 2.0;

  barier.max_x = room.getSize().x() / 2.0;
  barier.max_y = room.getSize().y() / 2.0;
  barier.max_z = room.getSize().z() / 2.0;

  auto materialData = static_cast<BricksMaterial*>(world.res().getMaterial("Wall")->getUniforms()->getData());
  materialData->scale = room.getSize();
  emit world.res().getMaterial("Wall")->getUniforms()->dataChanged(offsetof(BricksMaterial, scale), sizeof(QVector4D));

  room.forallEscapes([this] (const Room::DoorInstance &exitDoor) {
      auto exitEntity = bringDoor(exitDoor);
      inRoomEntities.push_back(exitEntity);

      exitEntity->onContact = [this] (Entity &/*self*/, Entity &e) {
        if(&e == &player->getEntity())
            goToRoom(roomIndex + 1);
        };
    });

  auto enterEntity = bringDoor(room.getEnter());
  //  enterEntity->onContact = [this, roomIndex] (Entity &e) {
  //      if(&e == &player->getEntity())
  //        goToRoom(roomIndex + 1);
  //    };

  inRoomEntities.push_back(enterEntity);
  player->getEntity().setPosition(enterEntity->getPosition() + enterEntity->getRotation().rotatedVector(QVector3D(0,0,1)));

  room.forallBonuses([this] (const Room::BonusInstance &bonus) {
      inRoomEntities.push_back(bringBonus(bonus));
    });

  room.forallKillers([this] (const Room::KillerInstance &killer) {
      inRoomKillers.push_back(new Killer(*this, killer));
    });

  room.forallStaticDecorations([this] (const Room::Decoration &decoration) {
      inRoomEntities.push_back(bringDecoration(decoration, ObjectType::Wall));
    });

  room.forallDynamicDecorations([this] (const Room::Decoration &decoration) {
      inRoomEntities.push_back(bringDecoration(decoration, ObjectType::Solid));
    });
}

void Game::goToNextRoom()
{
  goToRoom(roomIndex + 1);
}

void Game::goToPrevRoom()
{
  goToRoom(roomIndex - 1);
}

Entity *Game::bringKiller(const QString &type)
{
  auto el = killerTypes.find(type);
  if(el == killerTypes.end())
    throw std::invalid_argument("Game::bringKiller: killer type not exists");

  auto entity = world.bringEntity("Textured", el->modelName, ObjectType::Spirit);
  entity->onContact = [this] (Entity &/*self*/, Entity &e) {
    if(&e == &player->getEntity())
      finish();
  };
  return entity;
}

float Game::getKillerSpeed(const QString &type) const
{
  auto el = killerTypes.find(type);
  if(el == killerTypes.end())
    throw std::invalid_argument("Game::bringKiller: killer type not exists");

  return el->speed;
}

void Game::start()
{
  auto &illum = world.getIlluminationModel<KawaiiPbrLight>(0);

  illum.addDirLight();
  illum.changeDirLight(0, [](KawaiiIlluminationStructs<KawaiiPbrLight>::DirLight &l){
    l.direction = QVector3D(0, -1, 0);
    l.color = QVector3D(0.05,0.05,0.05);
    return true;
  });
  illum.addDotLight();
  illum.changeDotLight(0, [](KawaiiIlluminationStructs<KawaiiPbrLight>::Lamp &l){
    l.color = QVector3D(3, 3, 3);
    l.ambient = QVector3D(0.01, 0.01, 0.01);
    l.position = QVector3D(0,5,0);
    l.constAttenuation = 0;
    l.linearAttenuation = 1;
    l.quadraticAttenuation = 0.5;
    return true;
  });
  illum.addDotLight();
  illum.changeDotLight(1, [](KawaiiIlluminationStructs<KawaiiPbrLight>::Lamp &l){
    l.color = QVector3D(1.5, 1.5, 1.5);
    l.ambient = QVector3D(0.05, 0.05, 0.05);
    l.position = QVector3D(0,5,0);
    l.constAttenuation = 0;
    l.linearAttenuation = 1;
    l.quadraticAttenuation = 0.5;
    return true;
  });

  world.res().addInternalShaderEffect("colorTexture", ResourcePack::InternalShaderEffect::DiffuseTexturePbr, "PBR");
  world.res().createMaterial<SolidGlsl>("Textured", world.res().getShader("colorTexture"));
  world.res().loadModel(":/cube_x0.5", "Cube").first.waitForFinished();

  auto door = world.res().loadModel(":/cube_x0.5", "Door");
  door.first.waitForFinished();
  door.second->scale(QVector3D(0.75, doorHalfHeight = 2.5, 0.5));

  ShaderEffectSrc brickShaderSrc = {
    sib_utils::ioReadAll(QFile(":/shader_materials/bricks.vs.glsl")),
    sib_utils::ioReadAll(QFile(":/shader_materials/bricks.fs.glsl")),
    "PBR"
  };

  BricksMaterial wall = {QVector3D(50,50,50), QVector3D(0.75, 0.35, 0.3), QVector3D(0.3, 0.3, 0.3), QVector2D(0.25, 0.12), QVector2D(0.95, 0.95), QVector2D(0.05, 0.5), QVector2D(0.75, 0.5), -1};
  world.res().createMaterial<SolidGlsl>("Wall", brickShaderSrc, new KawaiiGpuBuf(&wall, sizeof(wall)));

  BricksMaterial doorMaterialData = {QVector3D(1,1,1), QVector3D(0.9, 0.9, 0.42), QVector3D(0.6, 0.6, 0.6), QVector2D(0.15, 0.15), QVector2D(0.7, 0.7), QVector2D(0.75, 0.3), QVector2D(0.75, 0.8), 1};
  world.res().createMaterial<SolidGlsl>("Door", brickShaderSrc, new KawaiiGpuBuf(&doorMaterialData, sizeof(doorMaterialData)));

  roomEntity = world.bringEntity("Wall", "Cube", ObjectType::None);
  roomEntity->setMass(0);


  world.res().waitTasks();

  player = std::make_unique<Character>(std::ref(world), std::ref(music));

  root.createRendererImplementation();
  world.startTime();

  KawaiiSurface *sfc = root.createChild<KawaiiSurface>(&player->getCamera());
  player->setInputObject(&sfc->getWindow());
  sfc->setFrametimeCounter(&currentFrametime);
  sfc->addOverlay(std::bind(&Game::drawOverlay, this, std::placeholders::_1));
  world.attachSurface(*sfc);

  goToRoom(0);
  sfc->showFullScreen();

  music.start();
}

void Game::finish()
{
  clearRoom();
  world.finishTime();
  player->releaseMouse();
  ending = music.ending(roomIndex);
}

void Game::drawOverlay(QPainter &painter)
{
  lastFrametimes[frameIndex] = currentFrametime;
  frameIndex = (frameIndex+1) % lastFrametimes.size();
  if(frameIndex == 0)
    {
      float fps = 0;
      for(const auto &i: lastFrametimes)
        fps += i;
      fps /= lastFrametimes.size();
      printf("Avg frametime = %f\n", fps);
    }

  if(ending == MusicMachine::State::Idle)
    {
      painter.setPen(Qt::white);
      int y = painter.window().height() - 30;
      painter.drawText(15, y, QStringLiteral("Room #%1").arg(roomIndex + 1));
      return;
    } else
    {
      painter.drawImage(QRect(0,0, painter.window().width(), painter.window().height()), *endings[ending]);
    }
}

void Game::clearRoom()
{
  for(auto *i: inRoomEntities)
    i->removeLater();
  inRoomEntities.clear();
  for(auto *i: inRoomKillers)
    delete i;
  inRoomKillers.clear();
}

const Room &Game::currentRoom() const
{
  return rooms[story[roomIndex]];
}

Entity *Game::bringDoor(const Room::DoorInstance &door)
{
  auto doorEntity = world.bringEntity("Door", "Door", ObjectType::Spirit);

  doorEntity->setLocation(QQuaternion::fromEulerAngles(0, door.rotation, 0),
                          QVector3D(door.position.x(),
                                    doorHalfHeight - 0.5 * currentRoom().getSize().y(),
                                    door.position.y()));

  return doorEntity;
}

Entity *Game::bringBonus(const Room::BonusInstance &bonus)
{
  const auto &bonusInfo = bonusTypes[bonus.type];

  auto bonusEntity = world.bringEntity("Textured", bonusInfo.modelName, ObjectType::Spirit);
  bonusEntity->setPosition(bonus.position);
  bonusEntity->setAngleVelocity(bonus.angleVel);

  bonusEntity->onContact = [this, &bonusInfo] (Entity &bonusEntity, Entity &e) {
      if(&e == &player->getEntity())
        {
          player->collectBonus(bonusInfo);
          auto el = std::find(inRoomEntities.begin(), inRoomEntities.end(), &bonusEntity);
          if(el != inRoomEntities.end())
            inRoomEntities.erase(el);
          bonusEntity.removeLater();
        }
    };

  return bonusEntity;
}

Entity *Game::bringDecoration(const Room::Decoration &decoration, ObjectType objType)
{
  auto entity = world.bringEntity("Textured", decoration.model, objType);
  entity->setPosition(decoration.pos0);
  entity->setVelocity(decoration.vel0);
  entity->setAngleVelocity(decoration.angleVel);
  entity->setMass(decoration.mass);
  entity->setFrictionFactor(0.15);
  return entity;
}
