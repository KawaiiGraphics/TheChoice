#ifndef BONUSINFO_HPP
#define BONUSINFO_HPP

#include <QString>

struct BonusInfo {
  QString modelName;

  enum class Effect: uint8_t {
    None,
    IncreaseHp,
    IncreaseKarma,
    DecreaseHp,
    DecreaseKarma,
  };

  Effect effect;
};

#endif // BONUSINFO_HPP
