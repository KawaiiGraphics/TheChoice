#include "MusicMachine.hpp"

const std::unordered_map<MusicMachine::State, QString>MusicMachine::stateNames = {
  { State::Idle, "IDLE" },
  { State::Scratch, "Scratch" },
  { State::Space, "Space" },
  { State::Dystopia, "Dystopia" },
  { State::Requiem, "Requiem" },
  { State::Cyberdance, "Cyberdance" }
};

MusicMachine::MusicMachine(World &world):
  world(world),
  state(State::Idle),
  music(nullptr),
  minRoomsToBest(-1),
  minRoomsToWorst(-1)
{
}

MusicMachine::~MusicMachine()
{
}

void MusicMachine::setMinRoomsToBest(int val)
{
  minRoomsToBest = val;
}

void MusicMachine::setMinRoomsToWorst(int val)
{
  minRoomsToWorst = val;
}

void MusicMachine::start()
{
  if(state == State::Idle)
    setState(State::Scratch);
}

void MusicMachine::acquireHP()
{
  switch(state)
    {
    case State::Idle: return;

    case State::Scratch:
      setState(State::Dystopia);
      break;

    case State::Space:
      setState(State::Cyberdance);
      break;

    case State::Dystopia:
      setState(State::Space);
      break;

    case State::Requiem:
      setState(State::Scratch);
      break;

    case State::Cyberdance:
      setState(State::Requiem);
      break;
    }
}

void MusicMachine::acquireKarma()
{
  switch(state)
    {
    case State::Idle: return;

    case State::Scratch:
      setState(State::Space);
      break;

    case State::Space:
      setState(State::Requiem);
      break;

    case State::Dystopia:
      setState(State::Cyberdance);
      break;

    case State::Requiem:
      setState(State::Space);
      break;

    case State::Cyberdance:
      setState(State::Scratch);
      break;
    }
}

void MusicMachine::lossHP()
{
  switch(state)
    {
    case State::Idle: return;

    case State::Scratch:
      setState(State::Requiem);
      break;

    case State::Space:
      setState(State::Requiem);
      break;

    case State::Dystopia:
      setState(State::Requiem);
      break;

    case State::Requiem:
      setState(State::Cyberdance);
      break;

    case State::Cyberdance:
      setState(State::Space);
      break;
    }
}

void MusicMachine::lossKarma()
{
  switch(state)
    {
    case State::Idle: return;

    case State::Scratch:
      setState(State::Cyberdance);
      break;

    case State::Space:
      setState(State::Dystopia);
      break;

    case State::Dystopia:
      setState(State::Cyberdance);
      break;

    case State::Requiem:
      setState(State::Dystopia);
      break;

    case State::Cyberdance:
      setState(State::Requiem);
      break;
    }
}

MusicMachine::State MusicMachine::ending(int roomsTravelled)
{
  saveCurrentStateDuration();

  uint64_t maxDuration = 0;
  State result = State::Idle;
  for(const auto &i: stateDurations)
    if(i.second > maxDuration && i.first != State::Idle)
      {
        if(i.first == State::Cyberdance && roomsTravelled < minRoomsToBest)
          continue;

        if(i.first == State::Requiem && roomsTravelled < minRoomsToWorst)
          continue;

        result = i.first;
        maxDuration = i.second;
      }

  if(music)
    music->stop();
  music = world.res().getSound(stateNames.at(state=result));
  if(music)
    music->play(std::chrono::milliseconds(0), 25);

  return result;
}

void MusicMachine::setState(MusicMachine::State st)
{
  saveCurrentStateDuration();
  state = st;

  if(music)
    music->stop();

  music = world.res().getSound(stateNames.at(state));

  if(music)
    music->play(music->random_pos, 25);
}

void MusicMachine::saveCurrentStateDuration()
{
  auto t = std::chrono::steady_clock::now();
  const uint64_t duration = std::chrono::duration_cast<std::chrono::milliseconds>(t - lastStateChange).count();
  auto el = stateDurations.find(state);
  if(el == stateDurations.end())
    el = stateDurations.insert( { state, 0 } ).first;
  el->second += duration;

  lastStateChange = t;
}
