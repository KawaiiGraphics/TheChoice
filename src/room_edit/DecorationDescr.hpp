#ifndef DECORATIONDESCR_HPP
#define DECORATIONDESCR_HPP

#include <QString>
#include <QVector3D>

struct DecorationDescr
{
  QString model;
  QVector3D pos0;
  QVector3D vel;
  QVector3D angleVel;
  float mass;

  bool dynamic;
  bool selected;
};

#endif // DECORATIONDESCR_HPP
