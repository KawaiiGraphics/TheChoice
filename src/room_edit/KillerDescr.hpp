#ifndef KILLERDESCR_HPP
#define KILLERDESCR_HPP

#include <QString>
#include <QVector3D>
#include <vector>

struct KillerDescr
{
  QString type;
  std::vector<QVector3D> route;
  QVector3D angleVel;

  bool selected;
};

#endif // KILLERDESCR_HPP
