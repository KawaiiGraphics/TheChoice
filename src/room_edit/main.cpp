#include <QApplication>
#include "RoomEditor.hpp"

int main(int argc, char *argv[])
{
  QApplication app(argc, argv);
  RoomEditor wnd;
  wnd.show();
  return app.exec();
}
