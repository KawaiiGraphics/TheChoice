#include "RoomEditor.hpp"
#include "ui_RoomEditor.h"
#include <QVector2D>
#include <QFileDialog>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

RoomEditor::RoomEditor(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::RoomEditor),
  enter { QVector3D(0, -1.35, -5), 0.0f, false }
{
  ui->setupUi(this);
  ui->scrollAreaWidgetContents->setAutoFillBackground(true);

  updateView();
}

RoomEditor::~RoomEditor()
{
  objViewers.clear();
  delete ui;
}

void RoomEditor::updateView()
{
  constexpr float scale = 50.0;
  constexpr float scale_x2 = scale * 2;

  objViewers.clear();

  const QSize sz(QSize(ui->spinRoomSizeX->value() * scale_x2, ui->spinRoomSizeZ->value() * scale_x2));
  const QVector2D sz_2(scale * ui->spinRoomSizeX->value(), scale * ui->spinRoomSizeZ->value());

  if(ui->scrollAreaWidgetContents->size() != sz)
    ui->scrollAreaWidgetContents->resize(sz);

  QFont f = font();
  f.setPointSize(32);

  if(auto old_fr = ui->scrollAreaWidgetContents->findChild<QFrame*>("GameArea"); old_fr)
    old_fr->deleteLater();

  QFrame *fr = new QFrame(ui->scrollAreaWidgetContents);
  fr->setObjectName("GameArea");
  fr->resize(sz);
  fr->setLineWidth(1);
  fr->setFrameShape(QFrame::Shape::Box);
  fr->show();

  auto moveObjView = [&sz_2] (QLabel &v, const QPointF &pos) {
    const QSize sz = v.sizeHint();

    const float x = (scale_x2 * pos.x()) + sz_2.x();
    const float y = (-scale_x2 * pos.y()) + sz_2.y() - 0.5 * sz.height();

    v.setGeometry(x, y, sz.width(), sz.height());
  };

  auto enterView = std::make_unique<ClickableLabel>("<span style=\"color:#ce6666;\">🚪</span>");
  enterView->onClick = std::bind(&RoomEditor::selectDoor, this, std::ref(enter));
  f.setBold(enter.selected);
  f.setItalic(enter.selected);
  enterView->setFont(f);
  enterView->setParent(ui->scrollAreaWidgetContents);
  enterView->show();
  moveObjView(*enterView, QPointF(enter.position.x(), enter.position.z()));
  objViewers.push_back(std::move(enterView));

  for(auto &i: bonuses)
    {
      auto objView = std::make_unique<ClickableLabel>("<span style=\"color:#33ce33;\">+</span>");
      objView->onClick = std::bind(&RoomEditor::selectBonus, this, std::ref(i));
      f.setBold(i.selected);
      f.setItalic(i.selected);
      objView->setFont(f);
      moveObjView(*objView, QPointF(i.position.x(), i.position.z()));
      objView->setParent(ui->scrollAreaWidgetContents);
      objView->show();
      objViewers.push_back(std::move(objView));
    }

  for(auto &i: killers)
    {
      auto objView = std::make_unique<ClickableLabel>("<span style=\"color:#ce3333;\">🔪</span>");
      objView->onClick = std::bind(&RoomEditor::selectKiller, this, std::ref(i));
      f.setBold(i.selected);
      f.setItalic(i.selected);
      objView->setFont(f);
      moveObjView(*objView, QPointF(i.route.front().x(), i.route.front().z()));
      objView->setParent(ui->scrollAreaWidgetContents);
      objView->show();
      objViewers.push_back(std::move(objView));
    }

  for(auto &i: exits)
    {
      auto objView = std::make_unique<ClickableLabel>("<span style=\"color:#66ce66;\">🚪</span>");
      objView->onClick = std::bind(&RoomEditor::selectDoor, this, std::ref(i));
      f.setBold(i.selected);
      f.setItalic(i.selected);
      objView->setFont(f);
      objView->setParent(ui->scrollAreaWidgetContents);
      objView->show();
      moveObjView(*objView, QPointF(i.position.x(), i.position.z()));
      objViewers.push_back(std::move(objView));
    }

  for(auto &i: decorations)
    {
      auto objView = std::make_unique<ClickableLabel>("<span style=\"color:#33ce33;\">+</span>");
      objView->onClick = std::bind(&RoomEditor::selectDecoration, this, std::ref(i));
      f.setBold(i.selected);
      f.setItalic(i.selected);
      objView->setFont(f);
      moveObjView(*objView, QPointF(i.pos0.x(), i.pos0.z()));
      objView->setParent(ui->scrollAreaWidgetContents);
      objView->show();
      objViewers.push_back(std::move(objView));
    }
}

void RoomEditor::clearSelection()
{
  enter.selected = false;
  for(auto &i: bonuses)
    i.selected = false;
  for(auto &i: killers)
    i.selected = false;
  for(auto &i: exits)
    i.selected = false;
  for(auto &i: decorations)
    i.selected = false;
}

void RoomEditor::selectBonus(BonusDescr &bonus)
{
  bool wasSelected = bonus.selected;
  clearSelection();
  bonus.selected = !wasSelected;

  ui->spinPosX->setValue(bonus.position.x());
  ui->spinPosY->setValue(bonus.position.y());
  ui->spinPosZ->setValue(bonus.position.z());

  ui->spinAngleVelX->setValue(bonus.angleVel.x());
  ui->spinAngleVelY->setValue(bonus.angleVel.y());
  ui->spinAngleVelZ->setValue(bonus.angleVel.z());

  ui->editType->setText(bonus.type);

  updateView();
}

void RoomEditor::selectKiller(KillerDescr &killer)
{
  bool wasSelected = killer.selected;
  clearSelection();
  killer.selected = !wasSelected;

  ui->spinPosX->setValue(killer.route.front().x());
  ui->spinPosY->setValue(killer.route.front().y());
  ui->spinPosZ->setValue(killer.route.front().z());

  ui->spinAngleVelX->setValue(killer.angleVel.x());
  ui->spinAngleVelY->setValue(killer.angleVel.y());
  ui->spinAngleVelZ->setValue(killer.angleVel.z());

  ui->editType->setText(killer.type);

  updateView();
}

void RoomEditor::selectDoor(DoorDescr &door)
{
  bool wasSelected = door.selected;
  clearSelection();
  door.selected = !wasSelected;

  ui->spinPosX->setValue(door.position.x());
  ui->spinPosY->setValue(door.position.y());
  ui->spinPosZ->setValue(door.position.z());

  ui->editType->setText(QString());

  updateView();
}

void RoomEditor::selectDecoration(DecorationDescr &decoration)
{
  bool wasSelected = decoration.selected;
  clearSelection();
  decoration.selected = !wasSelected;

  ui->spinPosX->setValue(decoration.pos0.x());
  ui->spinPosY->setValue(decoration.pos0.y());
  ui->spinPosZ->setValue(decoration.pos0.z());

  ui->spinAngleVelX->setValue(decoration.angleVel.x());
  ui->spinAngleVelY->setValue(decoration.angleVel.y());
  ui->spinAngleVelZ->setValue(decoration.angleVel.z());

  ui->editType->setText(decoration.model);

  updateView();
}

void RoomEditor::on_btnAddBonus_clicked()
{
  bonuses.push_back(BonusDescr { QString(), QVector3D(0,0,0), QVector3D(0,0,0), false });
  updateView();
}

void RoomEditor::on_btnAddKiller_clicked()
{
  killers.push_back(KillerDescr { QString(), { QVector3D(0,0,0) }, QVector3D(0,0,0), false });
  updateView();
}

void RoomEditor::on_btnAddExit_clicked()
{
  exits.push_back( DoorDescr { QVector3D(), 0, false } );
  updateView();
}

void RoomEditor::on_btnAddDecoration_clicked()
{
  decorations.push_back( DecorationDescr { QString(""), QVector3D(), QVector3D(), QVector3D(), 1.0, false, false } );
  updateView();
}

void RoomEditor::on_btnAddObject_clicked()
{
  decorations.push_back( DecorationDescr { QString(""), QVector3D(), QVector3D(), QVector3D(), 1.0, true, false } );
  updateView();
}

void RoomEditor::on_btnDel_clicked()
{
  enter.selected = false;
  for(auto i = bonuses.cbegin(); i != bonuses.cend(); )
    if(i->selected)
      i = bonuses.erase(i);
    else
      ++i;

  for(auto i = killers.cbegin(); i != killers.cend(); )
    if(i->selected)
      i = killers.erase(i);
    else
      ++i;

  for(auto i = exits.cbegin(); i != exits.cend(); )
    if(i->selected)
      i = exits.erase(i);
    else
      ++i;

  for(auto i = decorations.cbegin(); i != decorations.cend(); )
    if(i->selected)
      i = decorations.erase(i);
    else
      ++i;

  updateView();
}

void RoomEditor::on_spinRoomSizeX_valueChanged(double arg1)
{
  updateView();
  arg1 /= 2.0;
  ui->spinPosX->setRange(-arg1, arg1);
}

void RoomEditor::on_spinRoomSizeY_valueChanged(double arg1)
{
  arg1 /= 2.0;
  ui->spinPosY->setRange(-arg1, arg1);
}

void RoomEditor::on_spinRoomSizeZ_valueChanged(double arg1)
{
  updateView();
  arg1 /= 2.0;
  ui->spinPosZ->setRange(-arg1, arg1);
}

void RoomEditor::on_editType_textChanged(const QString &arg1)
{
  for(auto &i: bonuses)
    if(i.selected)
      i.type = arg1;

  for(auto &i: killers)
    if(i.selected)
      i.type = arg1;

  for(auto &i: decorations)
    if(i.selected)
      i.model = arg1;
}

void RoomEditor::on_spinPosX_valueChanged(double arg1)
{
  bool needUpdate = false;
  for(auto &i: bonuses)
    if(i.selected)
      {
        i.position.setX(arg1);
        needUpdate = true;
      }

  for(auto &i: killers)
    if(i.selected)
      {
        i.route.front().setX(arg1);
        needUpdate = true;
      }

  if(enter.selected)
    {
      enter.position.setX(arg1);
      needUpdate = true;
    }

  for(auto &i: exits)
    if(i.selected)
      {
        i.position.setX(arg1);
        needUpdate = true;
      }

  for(auto &i: decorations)
    if(i.selected)
      {
        i.pos0.setX(arg1);
        needUpdate = true;
      }

  if(needUpdate)
    updateView();
}

void RoomEditor::on_spinPosY_valueChanged(double arg1)
{
  for(auto &i: bonuses)
    if(i.selected)
      i.position.setY(arg1);

  for(auto &i: killers)
    if(i.selected)
      i.route.front().setY(arg1);

  if(enter.selected)
    enter.position.setY(arg1);

  for(auto &i: exits)
    if(i.selected)
      i.position.setY(arg1);

  for(auto &i: decorations)
    if(i.selected)
      i.pos0.setY(arg1);
}

void RoomEditor::on_spinPosZ_valueChanged(double arg1)
{
  bool needUpdate = false;
  for(auto &i: bonuses)
    if(i.selected)
      {
        i.position.setZ(arg1);
        needUpdate = true;
      }

  for(auto &i: killers)
    if(i.selected)
      {
        i.route.front().setZ(arg1);
        needUpdate = true;
      }

  if(enter.selected)
    {
      enter.position.setZ(arg1);
      needUpdate = true;
    }

  for(auto &i: exits)
    if(i.selected)
      {
        i.position.setZ(arg1);
        needUpdate = true;
      }

  for(auto &i: decorations)
    if(i.selected)
      {
        i.pos0.setZ(arg1);
        needUpdate = true;
      }

  if(needUpdate)
    updateView();
}

void RoomEditor::on_spinAngleVelX_valueChanged(double arg1)
{
  for(auto &i: bonuses)
    if(i.selected)
      i.angleVel.setX(arg1);

  for(auto &i: killers)
    if(i.selected)
      i.angleVel.setX(arg1);

  for(auto &i: decorations)
    if(i.selected)
      i.angleVel.setX(arg1);
}

void RoomEditor::on_spinAngleVelY_valueChanged(double arg1)
{
  for(auto &i: bonuses)
    if(i.selected)
      i.angleVel.setY(arg1);

  for(auto &i: killers)
    if(i.selected)
      i.angleVel.setY(arg1);

  for(auto &i: decorations)
    if(i.selected)
      i.angleVel.setY(arg1);
}

void RoomEditor::on_spinAngleVelZ_valueChanged(double arg1)
{
  for(auto &i: bonuses)
    if(i.selected)
      i.angleVel.setZ(arg1);

  for(auto &i: killers)
    if(i.selected)
      i.angleVel.setZ(arg1);

  for(auto &i: decorations)
    if(i.selected)
      i.angleVel.setZ(arg1);
}

namespace {
  QVector3D vec3FromJson(const QJsonArray &arr)
  {
    if(arr.size() < 3)
      return QVector3D();

    return QVector3D( arr.at(0).toDouble(), arr.at(1).toDouble(), arr.at(2).toDouble() );
  }

  DoorDescr doorFromJson(const QJsonObject &json)
  {
    const auto pos_v3 = vec3FromJson(json.value("position").toArray());
    return { QVector2D(pos_v3.x(), pos_v3.z()), static_cast<float>(json.value("rotation").toDouble(0.0)), false };
  }

  QJsonObject jsonFromDoor(const DoorDescr &door)
  {
    QJsonObject result;
    result.insert("position", QJsonArray({ door.position.x(), door.position.y(), door.position.z() }));
    if(door.rotation >= 0.01)
      result.insert("rotation", door.rotation);
    return result;
  }
}

void RoomEditor::on_actLoad_triggered()
{
  const QString fName = QFileDialog::getOpenFileName(this, QStringLiteral("Укажите файл комнаты"), QString(), QStringLiteral("JSON files (*.json);;All files (*)"));

  if(fName.isNull()) return;

  QFile f(fName);
  f.open(QFile::ReadOnly | QFile::Text);
  QJsonParseError json_err;
  QJsonDocument doc = QJsonDocument::fromJson(f.readAll(), &json_err);
  f.close();

  auto jsonObject = doc.object();

  auto size = vec3FromJson(jsonObject.value("size").toArray());

  ui->spinRoomSizeX->setValue(size.x());
  ui->spinRoomSizeY->setValue(size.y());
  ui->spinRoomSizeZ->setValue(size.z());

  enter = doorFromJson(jsonObject.value("enter").toObject());
  for(const auto &i: jsonObject.value("escape").toArray())
    exits.push_back(doorFromJson(i.toObject()));

  for(const auto &i: jsonObject.value("bonuses").toArray())
    {
      const auto obj = i.toObject();
      bonuses.push_back({ obj.value("type").toString(), vec3FromJson(obj.value("position").toArray()), vec3FromJson(obj.value("angle_vel").toArray()), false });
    }

  for(const auto &i: jsonObject.value("killers").toArray())
    {
      const auto obj = i.toObject();
      killers.push_back({});
      killers.back().type = obj.value("type").toString();
      killers.back().angleVel = vec3FromJson(obj.value("angle_vel").toArray());
      for(const auto &j: obj.value("path").toArray() )
        killers.back().route.push_back(vec3FromJson(j.toArray()));
    }

  for(const auto &i: jsonObject.value("static_decorations").toArray())
    {
      const auto obj = i.toObject();
      decorations.push_back({});
      decorations.back().model = obj.value("model").toString();
      decorations.back().pos0 = vec3FromJson(obj.value("pos0").toArray());
      decorations.back().vel = vec3FromJson(obj.value("vel0").toArray());
      decorations.back().mass = 1.0;
      decorations.back().angleVel = vec3FromJson(obj.value("angleVel").toArray());
    }

  for(const auto &i: jsonObject.value("dynamic_decorations").toArray())
    {
      const auto obj = i.toObject();
      decorations.push_back({});
      decorations.back().model = obj.value("model").toString();
      decorations.back().pos0 = vec3FromJson(obj.value("pos0").toArray());
      decorations.back().vel = vec3FromJson(obj.value("vel0").toArray());
      decorations.back().mass = obj.value("mass").toDouble(1.0);
      decorations.back().angleVel = vec3FromJson(obj.value("angleVel").toArray());
    }

  updateView();
}

void RoomEditor::on_actSave_triggered()
{
  const QString fName = QFileDialog::getSaveFileName(this, QStringLiteral("Укажите файл комнаты"), QString(), QStringLiteral("JSON files (*.json)"));

  if(fName.isNull()) return;

  QJsonObject jsonObject;
  jsonObject.insert("size", QJsonArray({ ui->spinRoomSizeX->value(), ui->spinRoomSizeY->value(), ui->spinRoomSizeZ->value() }));
  jsonObject.insert("enter", jsonFromDoor(enter));

  QJsonArray exits_json;
  for(const auto &i: exits)
    exits_json.push_back(jsonFromDoor(i));
  jsonObject.insert("escape", exits_json);

  QJsonArray bonuses_json;
  for(const auto &i: bonuses)
    {
      QJsonObject obj;
      obj.insert("type", i.type);
      obj.insert("position", QJsonArray({ i.position.x(), i.position.y(), i.position.z() }));
      obj.insert("angle_vel", QJsonArray({ i.angleVel.x(), i.angleVel.y(), i.angleVel.z() }));
      bonuses_json.push_back(obj);
    }
  if(!bonuses_json.empty())
    jsonObject.insert("bonuses", bonuses_json);

  QJsonArray killers_json;
  for(const auto &i: killers)
    {
      QJsonObject obj;
      obj.insert("type", i.type);
      obj.insert("angle_vel", QJsonArray({ i.angleVel.x(), i.angleVel.y(), i.angleVel.z()} ));
      QJsonArray path_json;
      for(const auto &j: i.route)
        path_json.push_back(QJsonArray({ j.x(), j.y(), j.z() }));
      obj.insert("path", path_json);

      killers_json.push_back(obj);
    }
  if(!killers_json.empty())
    jsonObject.insert("killers", killers_json);

  QJsonArray staticDecorations, dynamicDecorations;
  for(const auto &i: decorations)
    {
      QJsonObject obj;
      obj.insert("model", i.model);
      obj.insert("pos0", QJsonArray({ i.pos0.x(), i.pos0.y(), i.pos0.z() }));
      obj.insert("vel0", QJsonArray({ i.vel.x(), i.vel.y(), i.vel.z() }));
      obj.insert("angleVel", QJsonArray({ i.angleVel.x(), i.angleVel.y(), i.angleVel.z() }));
      if(i.dynamic)
        {
          dynamicDecorations.push_back(obj);
          obj.insert("mass", i.mass);
        } else
        staticDecorations.push_back(obj);
    }

  if(!staticDecorations.empty())
    jsonObject.insert("static_decorations", staticDecorations);

  if(!dynamicDecorations.empty())
    jsonObject.insert("dynamic_decorations", dynamicDecorations);

  QFile f(fName);
  f.open(QFile::WriteOnly | QFile::Text);
  f.write(QJsonDocument(jsonObject).toJson(QJsonDocument::JsonFormat::Indented));
  f.close();
}
