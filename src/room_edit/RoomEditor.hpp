#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <list>

#include "ClickableLabel.hpp"
#include "BonusDescr.hpp"
#include "KillerDescr.hpp"
#include "DoorDescr.hpp"
#include "DecorationDescr.hpp"

#include <memory>

namespace Ui {
  class RoomEditor;
}

class RoomEditor : public QMainWindow
{
  Q_OBJECT

public:
  explicit RoomEditor(QWidget *parent = nullptr);
  ~RoomEditor();

private slots:
  void on_btnAddBonus_clicked();

  void on_btnAddKiller_clicked();

  void on_btnAddExit_clicked();

  void on_btnAddDecoration_clicked();

  void on_btnAddObject_clicked();

  void on_btnDel_clicked();


  void on_spinRoomSizeX_valueChanged(double arg1);

  void on_spinRoomSizeY_valueChanged(double arg1);

  void on_spinRoomSizeZ_valueChanged(double arg1);

  void on_editType_textChanged(const QString &arg1);

  void on_spinPosX_valueChanged(double arg1);

  void on_spinPosY_valueChanged(double arg1);

  void on_spinPosZ_valueChanged(double arg1);

  void on_spinAngleVelX_valueChanged(double arg1);

  void on_spinAngleVelY_valueChanged(double arg1);

  void on_spinAngleVelZ_valueChanged(double arg1);

  void on_actLoad_triggered();

  void on_actSave_triggered();

private:
  Ui::RoomEditor *ui;

  std::vector<BonusDescr> bonuses;
  std::vector<KillerDescr> killers;
  std::vector<DoorDescr> exits;
  DoorDescr enter;
  std::vector<DecorationDescr> decorations;

  std::list<std::unique_ptr<ClickableLabel>> objViewers;

  void updateView();
  void clearSelection();

  void selectBonus(BonusDescr &bonus);
  void selectKiller(KillerDescr &killer);
  void selectDoor(DoorDescr &door);
  void selectDecoration(DecorationDescr &decoration);
};

#endif // MAINWINDOW_HPP
