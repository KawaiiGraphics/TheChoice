#include "ClickableLabel.hpp"

ClickableLabel::ClickableLabel(const QString &text):
  QLabel(text)
{
}

void ClickableLabel::mouseReleaseEvent(QMouseEvent *event)
{
  QLabel::mouseReleaseEvent(event);
  if(onClick)
    onClick();
}
